Mai jos pașii necesari rulării programului tip JSON:

-> se realizează configurația hardware
(Fără senzorii/componentele fizice utilizate, programul nu funcționează)

*pentru windows
-> se urmăresc pașii de instalare de la https://nodered.org/docs/getting-started/windows

*pentru linux
-> se instalează Node.js din terminal cu comanda 
"sudo apt install nodejs"
-> se instalează Node-RED din terminal cu comanda
"sudo npm install -g --unsafe-perm node-red"
-> se pornește din terminal cu comanda
"node-red"
-> se deschide browser web cu adresa http://localhost:1880
-> in interfața de utilizator, se intră la opțiuni și se accesează 
"Palette Manager"
-> se instalează următoarele pachete de noduri

• node-red 3.02 - nodurile de bază
• node-red-dashboard 3.5.0 - nodurile pentru tabloul de bord
• node-red-node-pi-gpio 2.0.6 - nodurile pentru interacțiunea cu pinii GPIO
• node-red-node-pisrf 0.2.0 – nodul pentru senzorul ultrasonic
• node-red-contrib-camerapi 0.0.39 – nodul pentru camera foto
• node-red-contrib-remote 1.4.0 – nodurile pentru aplicația mobilă
• node-red-contrib-dht-sensor 1.04 – nodul pentru senzorul de temperatură și umiditate
• node-red-contrib-pcf8574-lcd 0.1.0 – nodul pentru ecranul lcd
• node-red-contrib-simpletime 2.10.4 – nodul pentru un afișaj mai compact al timpului

-> se instalează Remote-RED pe telefonul mobil și se configurează aferent documentației
-> se face import la fișierul JSON (program licență)
-> se efectuează delpoy
-> se accesează interfața de utilizator la http://localhost:1880/ui
